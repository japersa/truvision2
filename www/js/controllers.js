angular.module('starter.controllers', [])

.controller('AppCtrl', function ($scope, $http, $ionicLoading, $timeout, $cordovaFileTransfer, $cordovaFileOpener2, $cordovaDialogs) {
    //server
    $scope.server = "http://managerpvto-001-site9.smarterasp.net/truvision.php";
    $scope.showProgress = false;
    $scope.downloadProgress = 0;
    // show loader download
    $scope.showLoaderDownload = function () {
        $ionicLoading.show({
            template: ' <ion-spinner icon="lines" class="spinner-calm"></ion-spinner><br>Cargando...',
            animation: 'fade-in',
            showBackdrop: false
        });
    }

    // hide loader download
    $scope.hideLoaderDownload = function () {
        $ionicLoading.hide();
    }

    // show loader submit form
    $scope.showLoaderSubmit = function () {
        $ionicLoading.show({
            template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner><br>Validando...',
            animation: 'fade-in',
            showBackdrop: false
        });
    }

    // hide loader submit form
    $scope.hideLoaderSubmit = function () {
        $ionicLoading.hide();
    }

    // show loader config
    $scope.showLoaderConfig = function () {
        $ionicLoading.show({
            template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner><br>Cargando...',
            animation: 'fade-in',
            showBackdrop: false
        });
    }

    // hide loader config
    $scope.hideLoaderConfig = function () {
        $ionicLoading.hide();
    }

    // load config download apk
    $scope.loadConfig = function () {
        // show loader 
        $scope.showLoaderConfig();

        // loading config data 
        $http.get($scope.server + "?config").then(function (result) {
            //hide loader 
            $scope.hideLoaderConfig();
            // result 
            if (result.data == 0) {
                // no configuration data
                $cordovaDialogs.alert('No hay parametros de configuracion.', 'Información', 'Aceptar')
                    .then(function () {
                        // callback success
                    });
            } else {

                // config data
                $scope.url = result.data.apk_url;
                $scope.filename = result.data.apk_nombre;
            }
        }, function (err) {
            //hide loader 
            $scope.hideLoaderConfig();
            $cordovaDialogs.alert('No se pudo conectar con el servidor.', 'Información', 'Aceptar')
                .then(function () {
                    // callback success
                });
        });
    }

    // download apk
    $scope.download = function (code) {
        var code_update = code.toUpperCase();
        //show loader 
        $scope.showLoaderSubmit();
    
        //submit form
        $http.get($scope.server + "?code=" + code_update).then(function (result) {
                //hide loader   
                $scope.hideLoaderSubmit();
                if (result.data == 0) {
                    $cordovaDialogs.alert('Codigo incorrecto.', 'Información', 'Aceptar')
                        .then(function () {
                            // callback success
                        });
                } else {

                    if (result.data.instalaciones < 2) {
                        // download apk
                        $cordovaDialogs.confirm('¿Deseas descargar la app?', 'Confirmación', ['Aceptar', 'Cancelar'])
                            .then(function (btn) {
                                if (btn == 1) {
                                    $scope.showLoaderDownload();

                                    // update number of downloads
                                    $http.get($scope.server + "?code_update=" + code_update).then(function (result) {
                                        $scope.hideLoaderDownload();
                                        if (result.data == 1) {
                                            $scope.showProgress = true;
                                            $scope.code = '';
                                            // download and open apk
                                            var url =  $scope.url;
                                            var targetPath = cordova.file.externalDataDirectory + $scope.filename;
                                            var trustHosts = true
                                            var options = {};

                                            $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
                                                .then(function (result) {
                                                    //open apk
                                                    $cordovaFileOpener2.open(
                                                        targetPath,
                                                        'application/vnd.android.package-archive'
                                                    ).then(function () {
                                                        $scope.showProgress = false;
                                                        $scope.downloadProgress = 0;
                                                    }, function (err) {
                                                        $scope.showProgress = false;
                                                        $scope.downloadProgress = 0;
                                                        $cordovaDialogs.alert('No se pudo abrir el archivo.', 'Información', 'Aceptar')
                                                            .then(function () {
                                                                // callback success
                                                            });
                                                    });
                                                }, function (err) {
                                                    $scope.showProgress = false;
                                                    // falied download apk
                                                    $cordovaDialogs.alert('No se pudo descargar el archivo.', 'Información', 'Aceptar')
                                                        .then(function () {
                                                            // callback success
                                                        });
                                                }, function (progress) {

                                                    $scope.downloadProgress = Math.round((progress.loaded / progress.total) * 100);
                                                });
                                        } else {

                                            // falied update number downloads
                                            $cordovaDialogs.alert('No se pudo descargar el archivo.', 'Información', 'Aceptar')
                                                .then(function () {
                                                    // callback success
                                                });
                                        }
                                    }, function (err) {
                                        $scope.hideLoaderDownload();

                                        $cordovaDialogs.alert('No se pudo descargar el archivo.', 'Información', 'Aceptar')
                                            .then(function () {
                                                // callback success
                                            });
                                    });
                                }
                            });
                    } else {
                        //hide loader
                        $scope.hideLoaderSubmit();
                        $cordovaDialogs.alert('Has superado el numero maximo de descargas.', 'Información', 'Aceptar')
                            .then(function () {
                                // callback success
                            });
                    }
                }
            },
            function (err) {
                //hide loader
                $scope.hideLoaderSubmit();
                $cordovaDialogs.alert('No se pudo conectar con el servidor.', 'Información', 'Aceptar')
                    .then(function () {
                        // callback success
                    });
            });
    }

    $scope.loadConfig();

});